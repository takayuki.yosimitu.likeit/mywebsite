<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン</title>
	<link rel="stylesheet" href="css/L.css">
</head>
	<body>

		<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t31.0-8/p960x960/16665001_1744218565894478_3874849047549629040_o.png?_nc_cat=111&_nc_sid=7aed08&_nc_ohc=YUUXosBEikkAX9zKX7O&_nc_ht=scontent-nrt1-1.xx&oh=9cf2fa4640df77bc47896ea67088c142&oe=5FB40C92" class="logo" alt="">

        <p class="h1">LOGIN</p>

        <a href="UserRegistrationServlet"><input type="button" value="NEW ACCOUNT" class="h7"></a>

        <c:if test="${errMsg != null}" >
        <a class=h8>${errMsg}</a>
　　　　　</c:if>

        <form action="LoginServlet" method="post">
            <p class="h2">LOGIN ID</p>
            <input type="text" name="loginId" class="h4">

             <p class="h3">PASSWORD</p>
            <input type="password" name="password" class="h5">

            <input type="submit" value="LOGIN" class="h6">
        </form>
	</body>
</html>
