package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ContentsBeans;
import beans.LikeitTweetBeans;
import dao.LikeitBulletinBordDao;
import dao.TableOfContentsDao;

/**
 * Servlet implementation class BulletinBoardServlet
 */
@WebServlet("/BulletinBoardServlet")
public class BulletinBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BulletinBoardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String contentsId = request.getParameter("contentsId");
    TableOfContentsDao TOCdao = new TableOfContentsDao();
    LikeitBulletinBordDao LBBdao = new LikeitBulletinBordDao();

	List<ContentsBeans> ContentsBeans = null;

	List<LikeitTweetBeans> TweetInfoList = LBBdao.findAll(contentsId);
	// リクエストスコープにセット
	request.setAttribute("TweetInfoList", TweetInfoList);
	try {
		ContentsBeans = TOCdao.findByAll(contentsId);
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
    for(ContentsBeans contents : ContentsBeans) {
      request.setAttribute("contents", contents);
    }
          int sum = 0;
		  int average = 0;
		  int num = TweetInfoList.size();
		for(LikeitTweetBeans Tweet : TweetInfoList) {
			sum += Tweet.getYouGood();
		    average = sum / num;

		// リクエストスコープにセット
		request.setAttribute("average", average);
		}
    	// ログインjspにフォワード
    				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Bulletinboard.jsp");
    				dispatcher.forward(request, response);
    	            return;
	}
}
