package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsultationTweetBeans implements Serializable{
	private int CtweetId;
	private String Name;
	private String Tweet;
	private Date TweetTime;
	private int GoodId;
	private String UserId;
	private int ContentsId;

	public ConsultationTweetBeans(int CtweetId, String Name, String Tweet, Date TweetTime, int GoodId, String UserId, int ContentsId) {
		this.CtweetId = CtweetId;
		this.Name = Name;
		this.Tweet = Tweet;
		this.TweetTime = TweetTime;
		this.GoodId = GoodId;
		this.UserId = UserId;
		this.ContentsId = ContentsId;
	}

	public int getCtweetId() {
		return CtweetId;
	}

	public void setCtweetId(int ctweetId) {
		CtweetId = ctweetId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getTweet() {
		return Tweet;
	}

	public void setTweet(String tweet) {
		Tweet = tweet;
	}

	public int getGoodId() {
		return GoodId;
	}

	public void setGoodId(int goodId) {
		GoodId = goodId;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public int getContentsId() {
		return ContentsId;
	}

	public void setContentsId(int contentsId) {
		ContentsId = contentsId;
	}

	public Date getTweetTime() {
		return TweetTime;
	}

	public void setTweetTime(Date tweetTime) {
		TweetTime = tweetTime;
	}
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(TweetTime);
	}
}
