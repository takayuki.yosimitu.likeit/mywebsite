package dao;

	import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.ConsultationTweetBeans;

	public class ConsultationBulletinBoardDao {
		/**
		 * Tweet情報を登録
		 * @return
		 * @throws NoSuchAlgorithmException
		 */
		public void InsertAll(String name, String tweet, String loginId,  String contentsId)
				throws NoSuchAlgorithmException {
			Connection conn = null;

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = " INSERT INTO consultation_tweet VALUES (NULL, ?, ?, CURRENT_TIMESTAMP, 0, ?, ?)";

				// SELECTを実行し、結果表を取得
				PreparedStatement Pstmt = conn.prepareStatement(sql);
				Pstmt.setString(1, name);
				Pstmt.setString(2, tweet);
				Pstmt.setString(3, loginId);
				Pstmt.setString(4, contentsId);

				Pstmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}

		/**
		 * 全てのユーザ情報を取得する
		 * @return
		 */
		public List<ConsultationTweetBeans> findAll(String contentsid) {
			Connection conn = null;
			List<ConsultationTweetBeans> TweetInfoList = new ArrayList<ConsultationTweetBeans>();

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM consultation_tweet WHERE contents_id = ? ORDER By Ctweet_id Desc";

				// SELECTを実行し、結果表を取得
							PreparedStatement Pstmt = conn.prepareStatement(sql);
							Pstmt.setString(1, contentsid);

							ResultSet rs = Pstmt.executeQuery();


				// 結果表に格納されたレコードの内容を
				// Userインスタンスに設定し、ArrayListインスタンスに追加
				while (rs.next()) {
					int CtweetId = rs.getInt("Ctweet_id");
					String name = rs.getString("name");
					String tweet = rs.getString("tweet");
					Date tweettime = rs.getDate("tweet_time");
					int goodId = rs.getInt("good_id");
					String userId = rs.getString("user_id");
					int contentsId = rs.getInt("contents_id");

					ConsultationTweetBeans TweetInfo = new ConsultationTweetBeans(CtweetId, name, tweet, tweettime, goodId, userId, contentsId);
					TweetInfoList.add(TweetInfo);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return TweetInfoList;
		}

		public void Update(int good, String CtweetId) {
			Connection conn = null;


			try {
				// データベースへ接続
				conn = DBManager.getConnection();

			    // SELECT文を準備
			    String sql = "UPDATE consultation_tweet SET good_id = ? WHERE Ctweet_id = ?";

			 // SELECTを実行し、結果表を取得
				PreparedStatement Pstmt = conn.prepareStatement(sql);
				Pstmt.setInt(1, good);
				Pstmt.setString(2, CtweetId);

				Pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				 }
			  }
		   }
		}

}
