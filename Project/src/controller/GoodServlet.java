package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BreakTweetBeans;
import beans.ConsultationTweetBeans;
import beans.ContentsBeans;
import beans.LikeitTweetBeans;
import dao.BreakBulletinBoardDao;
import dao.ConsultationBulletinBoardDao;
import dao.LikeitBulletinBordDao;
import dao.TableOfContentsDao;

/**
 * Servlet implementation class GoodServlet
 */
@WebServlet("/GoodServlet")
public class GoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの取得
				String LtweetId = request.getParameter("LtweetId");
				String CtweetId = request.getParameter("CtweetId");
				String BtweetId = request.getParameter("BtweetId");
				String LoginId = request.getParameter("loginId");
				String ContentsId = request.getParameter("contentsId");

				TableOfContentsDao TOCdao = new TableOfContentsDao();
				BreakBulletinBoardDao BBBdao = new BreakBulletinBoardDao();
				ConsultationBulletinBoardDao CBBdao = new ConsultationBulletinBoardDao();
				LikeitBulletinBordDao LBBdao = new LikeitBulletinBordDao();

				List<ContentsBeans> ContentsBeans = null;
				try {
					ContentsBeans = TOCdao.findByAll(ContentsId);
				} catch (NoSuchAlgorithmException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			    for(ContentsBeans contents : ContentsBeans) {
			    	request.setAttribute("contents", contents);
			    }

			    if(LtweetId != null) {
			    	try {
						TOCdao.InsertAll(LtweetId,CtweetId,BtweetId,LoginId);
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
					List<LikeitTweetBeans> TweetInfoList = LBBdao.findAll(ContentsId);
					  int sum = 0;
					  int average = 0;
					  int good = 0;
					  int number = TweetInfoList.size();
					for(LikeitTweetBeans Tweet : TweetInfoList) {
						sum += Tweet.getYouGood();
					    average = sum / number;
					// リクエストスコープにセット
					request.setAttribute("average", average);
					good = Tweet.getGoodId() + 1;
					LBBdao.Update(good, LtweetId);
					}
					List<LikeitTweetBeans> TweetInfoList1 = LBBdao.findAll(ContentsId);
					request.setAttribute("TweetInfoList", TweetInfoList1);
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Bulletinboard.jsp");
					dispatcher.forward(request, response);
			        return;


			    }else if(CtweetId != null) {
			    	try {
						TOCdao.InsertAll(LtweetId,CtweetId,BtweetId,LoginId);
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
					List<ConsultationTweetBeans> TweetInfoList = CBBdao.findAll(ContentsId);
					request.setAttribute("TweetInfoList", TweetInfoList);
					for(ConsultationTweetBeans Tweet : TweetInfoList) {
						int good = 0;
						good = Tweet.getGoodId() + 1;
						CBBdao.Update(good, CtweetId);
					}
					List<ConsultationTweetBeans> TweetInfoList1 = CBBdao.findAll(ContentsId);
					request.setAttribute("TweetInfoList", TweetInfoList1);
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BulletinboardConsultation.jsp");
					dispatcher.forward(request, response);
		            return;

			    }else if(BtweetId != null) {
			    	try {
						TOCdao.InsertAll(LtweetId,CtweetId,BtweetId,LoginId);
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
						List<BreakTweetBeans> TweetInfoList = BBBdao.findAll(ContentsId);
						request.setAttribute("TweetInfoList", TweetInfoList);
						for(BreakTweetBeans Tweet : TweetInfoList) {
							int good = 0;
							good = Tweet.getGoodId() + 1;
							BBBdao.Update(good, BtweetId);
						}
						List<BreakTweetBeans> TweetInfoList1 = BBBdao.findAll(ContentsId);
						request.setAttribute("TweetInfoList", TweetInfoList1);
						// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BulletinboardBreak.jsp");
						dispatcher.forward(request, response);
			            return;
		 }
	}
}