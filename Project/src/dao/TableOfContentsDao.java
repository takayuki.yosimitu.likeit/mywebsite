package dao;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ContentsBeans;
import beans.GoodBeans;

public class TableOfContentsDao {
	/**
	 * unit情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public List<ContentsBeans> findByAll(String contentsId) throws NoSuchAlgorithmException {
		List<ContentsBeans> ContentsBeansList = new ArrayList<ContentsBeans>();
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM contents WHERE contents_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, contentsId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			while (rs.next()) {
				int contentsIdData = rs.getInt("contents_id");
				String nameData = rs.getString("contents_name");
				int unitDate = rs.getInt("unit_id");
				ContentsBeans contentsbeans = new ContentsBeans(contentsIdData, nameData, unitDate);
				ContentsBeansList.add(contentsbeans);
			}

			// 必要なデータのみインスタンスのフィールドに追加

			return ContentsBeansList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * Tweet情報を登録
	 * @param
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public void InsertAll(String LtweetId , String CtweetId, String BtweetId, String loginId)
			throws NoSuchAlgorithmException {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			if(LtweetId != null) {
			String sql = " INSERT INTO good VALUES (NULL, ?, ?, NULL, NULL)";

			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, loginId);
			Pstmt.setString(2, LtweetId);
			Pstmt.executeUpdate();

			}else if(CtweetId != null) {
				String sql = " INSERT INTO good VALUES (NULL, ?, NULL, ?, NULL)";

				PreparedStatement Pstmt = conn.prepareStatement(sql);
				Pstmt.setString(1, loginId);
				Pstmt.setString(2, CtweetId);
				Pstmt.executeUpdate();

			}else if(BtweetId != null) {
				String sql = " INSERT INTO good VALUES (NULL, ?, NULL, NULL, ?)";

				PreparedStatement Pstmt = conn.prepareStatement(sql);
				Pstmt.setString(1, loginId);
				Pstmt.setString(2, BtweetId);
				Pstmt.executeUpdate();

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * LtweetIdに紐づく情報を返す
	 * @param LtweetId
	 * @return
	 */
	public List<GoodBeans> findByLtweetId(String ltweetId) {
		Connection conn = null;
		List<GoodBeans> GoodBeansList = new ArrayList<GoodBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM good WHERE Ltweet_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, ltweetId);
			ResultSet rs = Pstmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String Id = rs.getString("good_id");
				String loginId = rs.getString("user_id");
				String LtweetId = rs.getString("Ltweet_id");
				String CtweetId = rs.getString("Ctweet_id");
				String BtweetId = rs.getString("Btweet_id");

				GoodBeans user = new GoodBeans(Id, loginId,LtweetId, CtweetId, BtweetId);

				GoodBeansList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return GoodBeansList;
	}

	/**
	 * CtweetIdに紐づく情報を返す
	 * @param CtweetId
	 * @return
	 */
	public List<GoodBeans> findByCtweetId(String ctweetId) {
		Connection conn = null;
		List<GoodBeans> GoodBeansList = new ArrayList<GoodBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM good WHERE Ctweet_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, ctweetId);
			ResultSet rs = Pstmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String Id = rs.getString("good_id");
				String loginId = rs.getString("user_id");
				String LtweetId = rs.getString("Ltweet_id");
				String CtweetId = rs.getString("Ctweet_id");
				String BtweetId = rs.getString("Btweet_id");

				GoodBeans user = new GoodBeans(Id, loginId,LtweetId, CtweetId, BtweetId);

				GoodBeansList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return GoodBeansList;
	}


	/**
	 * BtweetIdに紐づくユーザ情報を返す
	 * @param BtweetId
	 * @return
	 */
	public List<GoodBeans> findByBtweetId(String btweetId) {
		Connection conn = null;
		List<GoodBeans> GoodBeansList = new ArrayList<GoodBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM good WHERE Btweet_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, btweetId);
			ResultSet rs = Pstmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String Id = rs.getString("good_id");
				String loginId = rs.getString("user_id");
				String LtweetId = rs.getString("Ltweet_id");
				String CtweetId = rs.getString("Ctweet_id");
				String BtweetId = rs.getString("Btweet_id");

				GoodBeans user = new GoodBeans(Id, loginId,LtweetId, CtweetId, BtweetId);

				GoodBeansList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return GoodBeansList;
	}


}
