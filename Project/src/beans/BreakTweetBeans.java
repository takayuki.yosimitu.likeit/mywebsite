package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BreakTweetBeans implements Serializable{
	private int BtweetId;
	private String Name;
	private String Tweet;
	private Date TweetTime;
	private int GoodId;
	private String UserId;
	private int ContentsId;

	public BreakTweetBeans(int BtweetId, String Name, String Tweet, Date tweettime, int GoodId, String UserId, int ContentsId) {
		this.BtweetId = BtweetId;
		this.Name = Name;
		this.Tweet = Tweet;
		this.TweetTime = tweettime;
		this.GoodId = GoodId;
		this.UserId = UserId;
		this.ContentsId = ContentsId;
	}

	public int getBtweetId() {
		return BtweetId;
	}

	public void setBtweetId(int btweetId) {
		BtweetId = btweetId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getTweet() {
		return Tweet;
	}

	public void setTweet(String tweet) {
		Tweet = tweet;
	}

	public int getGoodId() {
		return GoodId;
	}

	public void setGoodId(int goodId) {
		GoodId = goodId;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public int getContentsId() {
		return ContentsId;
	}

	public void setContentsId(int contentsId) {
		ContentsId = contentsId;
	}

	public Date getTweetTime() {
		return TweetTime;
	}

	public void setTweetTime(Date tweetTime) {
		TweetTime = tweetTime;
	}
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(TweetTime);
	}

}
