<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー更新（完了）</title>
	<link rel="stylesheet" href="css/UUD.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>

        <a href="UserUpdateServlet?loginId=${userInfo.loginId}"><p class="h3">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h4">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h5">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h6">⍈</p></a>

        <p class="h2">更新が完了しました。</p>



	</body>
</html>