<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>目次</title>
	<link rel="stylesheet" href="css/TOC.css">
</head>

	<body>
        <div class="line"></div>
        <p class="h1">LIKEIT</p>

        <a href="TableOfContentsServlet?unitId=1">
        <div class="line1"></div>
        <p class="h3">Java基礎</p>
        </a>

        <a href="TableOfContentsServlet?unitId=2">
        <div class="line2"></div>
        <p class="h4">SQL</p>
        </a>

        <a href="TableOfContentsServlet?unitId=3">
        <div class="line3"></div>
        <p class="h5">Webプログラミング</p>
        </a>

        <a href="TableOfContentsServlet?unitId=4">
        <div class="line4"></div>
        <p class="h6">リファクタリング</p>
        </a>


        <a href="TableOfContentsServlet?unitId=5">
        <div class="line5"></div>
        <p class="h7">個人開発</p>
        </a>

        <a href="TableOfContentsServlet?unitId=6">
        <div class="line6"></div>
        <p class="h8">目安箱</p>
        </a>

        <a href="TableOfContentsServlet?unitId=7">
        <div class="line7"></div>
        <p class="h9">休憩時間の過ごし方</p>
        </a>

        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h10">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h11">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h12">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h13">⍈</p></a>

	</body>
</html>