package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public UserUpdateServlet() {
	        super();
	        // TODO Auto-generated constructor stub
	    }

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// URLからGETパラメータとしてIDを受け取る
			String LoginId = request.getParameter("loginId");

	        //idを引数にして、idに紐づくユーザ情報を出力する
			UserDao userdao = new UserDao();
			List<UserBeans> userlist = userdao.findByLoginId(LoginId);

			for(UserBeans user : userlist) {

			// ユーザ情報をリクエストスコープにセットしてjspにフォワード
	         user.setId(user.getId());
	         user.setLoginId(user.getLoginId());
	         user.setName(user.getName());
	         user.setBirthDate(user.getBirthDate());
	         user.setCreateDate(user.getCreateDate());
	         user.setUpdateDate(user.getUpdateDate());

	         request.setAttribute("user", user);
	      // JSPへフォワード
	         RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
	         dispatcher.forward(request, response);
	         return;
			}
		}
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			/**
			 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
			 *      response)
			 */
			        // リクエストパラメータの文字コードを指定
			        request.setCharacterEncoding("UTF-8");

					// リクエストパラメータの入力項目を取得
			        String loginId = request.getParameter("loginId");
					String name = request.getParameter("name");
					String birthdate = request.getParameter("birthdate");
					String password1 = request.getParameter("password1");
					String password2 = request.getParameter("password2");

					//idを引数にして、idに紐づくユーザ情報を出力する
					UserDao userdao = new UserDao();
					List<UserBeans> userlist = userdao.findByloginid(loginId);

					for(UserBeans user : userlist) {

					/** テーブルに該当のデータが見つからなかった場合 **/
					if(name == "" && birthdate == "" && password1 == "" && password2 == "") {
						// リクエストスコープにエラーメッセージをセット
						request.setAttribute("errMsg", "入力されていません。");
						request.setAttribute("user", user);
						// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
						dispatcher.forward(request, response);
						return;
					}
						// セッションにユーザの情報をセット
						userdao.Update(name, birthdate, password1,loginId);

						 // ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate(Done).jsp");
						dispatcher.forward(request, response);
						return;
					}
					}
}