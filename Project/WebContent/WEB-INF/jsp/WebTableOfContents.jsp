<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>目次（Web）</title>
	<link rel="stylesheet" href="css/WTOC.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h10">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h11">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h12">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h13">⍈</p></a>

        <div class="line1"></div>
        <p class="h3">Webプログラミング</p>


        <a href="BulletinBoardServlet?contentsId=301"><p class="h4">1.Webページの仕組み</p></a>
        <a href="BulletinBoardServlet?contentsId=302"><p class="h5">2.サーブレットとJSPの基礎</p></a>
        <a href="BulletinBoardServlet?contentsId=303"><p class="h6">3.Webアプリケーションの開発（基礎）</p></a>
        <a href="BulletinBoardServlet?contentsId=304"><p class="h7">4.DBとの連携</p></a>
        <a href="BulletinBoardServlet?contentsId=305"><p class="h8">5.モックの作り方</p></a>
        <a href="BulletinBoardServlet?contentsId=306"><p class="h9">6.Web総合課題</p></a>

	</body>
</html>