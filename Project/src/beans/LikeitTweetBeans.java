package beans;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

	public class LikeitTweetBeans implements Serializable{
		private int LtweetId;
		private String Name;
		private String Tweet;
		private Date TweetTime;
		private int YouGood;
		private int GoodId;
		private String UserId;
		private int ContentsId;

		public LikeitTweetBeans(int LtweetId, String Name, String Tweet, Date TweetTime, int YouGood, int GoodId, String userId, int ContentsId) {
			this.LtweetId = LtweetId;
			this.Name = Name;
			this.Tweet = Tweet;
			this.TweetTime = TweetTime;
			this.YouGood = YouGood;
			this.GoodId = GoodId;
			this.UserId = userId;
			this.ContentsId = ContentsId;

		}

		public int getLtweetId() {
			return LtweetId;
		}

		public void setLtweetId(int ltweetId) {
			LtweetId = ltweetId;
		}

		public String getName() {
			return Name;
		}

		public void setName(String name) {
			Name = name;
		}

		public String getTweet() {
			return Tweet;
		}

		public void setTweet(String tweet) {
			Tweet = tweet;
		}

		public int getYouGood() {
			return YouGood;
		}

		public void setYouGood(int youGood) {
			YouGood = youGood;
		}

		public int getGoodId() {
			return GoodId;
		}

		public void setGoodId(int goodId) {
			GoodId = goodId;
		}

		public String getUserId() {
			return UserId;
		}

		public void setUserId(String userId) {
			UserId = userId;
		}

		public int getContentsId() {
			return ContentsId;
		}

		public void setContentsId(int contentsId) {
			ContentsId = contentsId;
		}

		public Date getTweetTime() {
			return TweetTime;
		}

		public void setTweetTime(Date tweetTime) {
			TweetTime = tweetTime;
		}
		public String getFormatDate() {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
			return sdf.format(TweetTime);
		}

	}

