<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー登録</title>
	<link rel="stylesheet" href="css/UR.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet"><p class="h12">«</p></a>

        <c:if test="${errMsg != null}">
		  <p class="h16">${errMsg}</p>
		</c:if>


        <form action="UserRegistrationServlet" method="post">
            <p class="h2">ログイン ID</p>
            <input type="text" name="loginId" class="h7">

            <p class="h3">ニックネーム</p>
            <input type="text" name="name" class="h8">

            <p class="h4">パスワード</p>
            <input type="password" name="password1" class="h9">

            <p class="h5">パスワード（確認）</p>
            <input type="password" name="password2" class="h10">

            <p class="h6">生年月日</p>
            <input type="date" name="birthdate" class="h11">

            <input type="submit" value="登録" class="h15">
        </form>
	</body>
</html>
