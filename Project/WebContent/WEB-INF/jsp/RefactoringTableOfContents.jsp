<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>目次（リファクタリング）</title>
	<link rel="stylesheet" href="css/RTOC.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h6">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h7">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h8">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h9">⍈</p></a>

        <div class="line1"></div>
        <p class="h3">リファクタリング</p>


        <a href="BulletinBoardServlet?contentsId=401"><p class="h4">1.バグ改修</p></a>
        <a href="BulletinBoardServlet?contentsId=402"><p class="h5">2.追加実装</p></a>

	</body>
</html>