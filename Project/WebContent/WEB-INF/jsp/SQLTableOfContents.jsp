<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>目次（SQL）</title>
	<link rel="stylesheet" href="css/STOC.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h9">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h10">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h11">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h12">⍈</p></a>


        <div class="line1"></div>
        <p class="h3">SQL</p>


        <a href="BulletinBoardServlet?contentsId=201"><p class="h4">1.DBを始めよう</p></a>
        <a href="BulletinBoardServlet?contentsId=202"><p class="h5">2.SQL基礎</p></a>
        <a href="BulletinBoardServlet?contentsId=203"><p class="h6">3.正規化</p></a>
        <a href="BulletinBoardServlet?contentsId=204"><p class="h7">4.SQL応用</p></a>
        <a href="BulletinBoardServlet?contentsId=205"><p class="h8">5.SQL総合問題</p></a>
	</body>
</html>