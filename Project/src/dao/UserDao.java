package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserBeans;

public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public UserBeans findByLoginInfo(String loginId, String password) throws NoSuchAlgorithmException {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, source(password));
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new UserBeans(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<UserBeans> findAll() {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				UserBeans user = new UserBeans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * IDに紐づくユーザ情報を返す
	 * @param id
	 * @return
	 */
	public List<UserBeans> findById(String id) {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, id);
			ResultSet rs = Pstmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int Id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				UserBeans user = new UserBeans(Id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * IDに紐づくユーザ情報を返す
	 * @param id
	 * @return
	 */
	public List<UserBeans> findByLoginId(String LoginId) {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, LoginId);
			ResultSet rs = Pstmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int Id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				UserBeans user = new UserBeans(Id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ログインIDとパスワードとユーザー名と生年月日に紐づくユーザ情報を登録
	 * @param loginId
	 * @param password
	 * @param username
	 * @param birthDate
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public void InsertAll(String loginid, String password, String username, String birthDate)
			throws NoSuchAlgorithmException {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = " INSERT INTO user VALUES (NULL, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, loginid);
			Pstmt.setString(4, source(password));
			Pstmt.setString(2, username);
			Pstmt.setString(3, birthDate);

			Pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * ログインIDに紐づくユーザ情報を削除
	 * @param loginId
	 * @return
	 */
	public void DeleteByloginId(String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "DELETE FROM user WHERE login_Id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, loginId);

			Pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * ログインIDに紐づくユーザ情報を返す
	 * @param loginId
	 * @return
	 */
	public String findByloginId(String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement Pstmt = conn.prepareStatement(sql);
			Pstmt.setString(1, loginId);

			ResultSet rs = Pstmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginid = rs.getString("login_id");

			return loginid;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 暗号化
	 * @param
	 * @return
	 * @throws
	 */
	public String source(String pass) throws NoSuchAlgorithmException {

		//ハッシュを生成したい元の文字列
		String source = pass;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		return result;
	}

public List<UserBeans> findByloginid(String loginid) {
	Connection conn = null;
	List<UserBeans> userList = new ArrayList<UserBeans>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM user WHERE login_id = ?";

		// SELECTを実行し、結果表を取得
		PreparedStatement Pstmt = conn.prepareStatement(sql);
		Pstmt.setString(1, loginid);
		ResultSet rs = Pstmt.executeQuery();

		// 結果表に格納されたレコードの内容を
		// Userインスタンスに設定し、ArrayListインスタンスに追加
		while (rs.next()) {
			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			UserBeans user = new UserBeans(Id, loginId, name, birthDate, password, createDate, updateDate);

			userList.add(user);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return userList;
}

public void Update(String Name, String birthdate, String Password, String LoginId) {
Connection conn = null;

try {
    // データベースへ接続
    conn = DBManager.getConnection();
    String SQL = null;

    if(!Name .equals("")) {
    	SQL = "name = '"+Name+"'";
    }

    if(!birthdate.equals("")) {
    	SQL = "birth_date = '"+birthdate+"'";
    }

    if(!Password.equals("")) {
    	SQL = "password = '"+Password+"'";
    }

    if(!Name.equals("") && !birthdate.equals("")) {
    	SQL = "name = '"+Name+"' , birth_date = '"+birthdate+"'";
    }
    if(!Name.equals("") && !Password.equals("")) {
    	SQL = "name = '"+Name+"' , birth_date = '"+birthdate+"'";
    }
    if(!Password.equals("") && !birthdate.equals("")) {
    	SQL = "password = '"+Password+"' , birth_date = '"+birthdate+"'";
    }

    if(!Name.equals("") && !Password.equals("") && !birthdate.equals("")) {
        SQL = "name= '"+Name+"' , Password = '"+Password+"' , birth_date = '"+birthdate+"'";
    }

    // SELECT文を準備
    String sql = "UPDATE user SET "+SQL+" ,update_date = CURRENT_TIMESTAMP WHERE login_id = '"+LoginId+"'";


     // SELECTを実行し、結果表を取得
	Statement stmt = conn.createStatement();
	stmt.executeUpdate(sql);

} catch (SQLException e) {
	e.printStackTrace();
} finally {
	// データベース切断
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
}


}