<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>目次（Java）</title>
	<link rel="stylesheet" href="css/JTOC.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>

        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h22">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h23">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h24">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h25">⍈</p></a>

        <div class="line1"></div>
        <p class="h3">Java基礎</p>


<a href="BulletinBoardServlet?contentsId=101"><p class="h4">1.Javaを始めよう</p></a>
<a href="BulletinBoardServlet?contentsId=102"><p class="h5">2.プログラミングの基本構造</p></a>
<a href="BulletinBoardServlet?contentsId=103"><p class="h6">3.変数・式と演算子</p></a>
<a href="BulletinBoardServlet?contentsId=104"><p class="h7">4.条件分岐</p></a>
<a href="BulletinBoardServlet?contentsId=105"><p class="h8">5.繰り返し</p></a>
<a href="BulletinBoardServlet?contentsId=106"><p class="h9">6.配列</p></a>
<a href="BulletinBoardServlet?contentsId=107"><p class="h10">7.メソッド</p></a>
<a href="BulletinBoardServlet?contentsId=108"><p class="h11">8.複数のクラスを用いた開発</p></a>
<a href="BulletinBoardServlet?contentsId=109"><p class="h12">9.オブジェクト指向</p></a>
<a href="BulletinBoardServlet?contentsId=110"><p class="h13">10.インスタンスとクラス</p></a>
<a href="BulletinBoardServlet?contentsId=111"><p class="h14">11.さまざまなクラス機構</p></a>
<a href="BulletinBoardServlet?contentsId=112"><p class="h15">12.カプセル化</p></a>
<a href="BulletinBoardServlet?contentsId=113"><p class="h16">13.継承</p></a>
<a href="BulletinBoardServlet?contentsId=114"><p class="h17">14.高度な継承</p></a>
<a href="BulletinBoardServlet?contentsId=115"><p class="h18">15.多様性</p></a>
<a href="BulletinBoardServlet?contentsId=116"><p class="h19">16.標準クラス</p></a>
<a href="BulletinBoardServlet?contentsId=117"><p class="h20">17.例外</p></a>
<a href="BulletinBoardServlet?contentsId=118"><p class="h21">18.コレクション</p></a>

	</body>
</html>