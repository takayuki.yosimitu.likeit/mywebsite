package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BreakTweetBeans;
import beans.ConsultationTweetBeans;
import beans.ContentsBeans;
import dao.BreakBulletinBoardDao;
import dao.ConsultationBulletinBoardDao;
import dao.TableOfContentsDao;


/**
 * Servlet implementation class TableOfContentsServlet
 */
@WebServlet("/TableOfContentsServlet")
public class TableOfContentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TableOfContentsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String UnitId = request.getParameter("unitId");

		TableOfContentsDao TOCdao = new TableOfContentsDao();

				if(UnitId .equals("1")) {
			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/JavaTableOfContents.jsp");
			dispatcher.forward(request, response);
	        return;
				}else if(UnitId .equals("2")){
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SQLTableOfContents.jsp");
					dispatcher.forward(request, response);
			        return;

				}else if(UnitId .equals("3")){
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/WebTableOfContents.jsp");
					dispatcher.forward(request, response);
			        return;

				}else if(UnitId .equals("4")){
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/RefactoringTableOfContents.jsp");
					dispatcher.forward(request, response);
			        return;

				}else if(UnitId .equals("5")){
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/PersonalDevelopmentTableOfContents.jsp");
					dispatcher.forward(request, response);
			        return;

				}else if(UnitId .equals("6")){
					List<ContentsBeans> ContentsBeans = null;
					ConsultationBulletinBoardDao CBBdao = new ConsultationBulletinBoardDao();
					String ContentsId = "601";
					try {
						ContentsBeans = TOCdao.findByAll(ContentsId);
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
					for(ContentsBeans contents : ContentsBeans) {
						request.setAttribute("contents", contents);
					}
						List<ConsultationTweetBeans> TweetInfoList = CBBdao.findAll(ContentsId);
						request.setAttribute("TweetInfoList", TweetInfoList);
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BulletinboardConsultation.jsp");
					dispatcher.forward(request, response);
			        return;

				}else if(UnitId.equals("7")){
					List<ContentsBeans> ContentsBeans = null;
					  BreakBulletinBoardDao BBBdao = new BreakBulletinBoardDao();
					String ContentsId = "701";
					try {
						ContentsBeans = TOCdao.findByAll(ContentsId);
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
					for(ContentsBeans contents : ContentsBeans) {
						// リクエストスコープにセット
				        request.setAttribute("contents", contents);
					}
						List<BreakTweetBeans> TweetInfoList = BBBdao.findAll(ContentsId);
						request.setAttribute("TweetInfoList", TweetInfoList);
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BulletinboardBreak.jsp");
					dispatcher.forward(request, response);
			        return;
		}
	}
}