package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserRegistrationVerificationServlet
 */
@WebServlet("/UserRegistrationVerificationServlet")
public class UserRegistrationVerificationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegistrationVerificationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String LoginId = request.getParameter("loginId");
		String Password = request.getParameter("password");
		String Name = request.getParameter("name");
		String BirthDate = request.getParameter("birthDate");

		UserDao userdao = new UserDao();
         try {

			userdao.InsertAll(LoginId, Password, Name, BirthDate);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
 		UserBeans user = null;
		try {
			user = userdao.findByLoginInfo(LoginId, Password);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

      // セッションにユーザの情報をセット
 		HttpSession session = request.getSession();
 		session.setAttribute("userInfo", user);
      // JSPへフォワード
         RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration(Done).jsp");
         dispatcher.forward(request, response);
           return;
		}
}
