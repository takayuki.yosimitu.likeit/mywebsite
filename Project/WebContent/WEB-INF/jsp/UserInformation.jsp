<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー情報</title>
	<link rel="stylesheet" href="css/UI.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h12">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h13">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h14">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h15">⍈</p></a>
       <%--  <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h16">⌦</p></a> --%>

            <p class="h2">ログイン ID</p>
            <p class="h7">${user.loginId}</p>
            <input type="hidden" name="loginId" value="${user.loginId}">

            <p class="h3">ニックネーム</p>
            <p class="h8">${user.name}</p>

            <%-- <p class="h4">パスワード</p>
            <p class="h9">${user.password}</p>

            <p class="h5">パスワード（確認）</p>
            <p class="h10">${user.password}</p> --%>

            <p class="h4">生年月日</p>
            <p class="h9">${user.birthDate}</p>

            <a href="UserUpdateServlet?loginId=${userInfo.loginId}"><input type="button" value="更新" class="h17"></a>

	</body>
</html>
