package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserRegistrationServlet
 */
@WebServlet("/UserRegistrationServlet")
public class UserRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ユーザー登録画面のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの取得
		String loginId = request.getParameter("loginId");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");

		// ユーザ一覧情報を登録
		UserDao userdao = new UserDao();
		String loginid = userdao.findByloginId(loginId);

		/** 既に登録されているログインIDが入力された場合。
		　- パスワードとパスワード(確認)の入力内容が異なる場合
		　- 入力項目に1つでも未入力のものがある場合*/
		if (loginid != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインIDは既に使用されています。");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (loginId == "") {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインIDが入力されていません。");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (password1 == "") {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが入力されていません。");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (password2 == "") {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワード(確認)が入力されていません");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (name == "") {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ユーザー名が入力されていません");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (birthdate == "") {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "生年月日が入力されていません。");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (!(password1.equals(password2))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが正しくありません。");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
		    UserBeans userbeans = new UserBeans(loginId, name, birthdate, password1);
		    request.setAttribute("userbeans", userbeans);
		// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegistration(Verification).jsp");
		    dispatcher.forward(request, response);
				return;
		}
	}
}