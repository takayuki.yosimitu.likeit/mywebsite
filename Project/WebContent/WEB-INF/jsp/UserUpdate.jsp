<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー更新</title>
	<link rel="stylesheet" href="css/UU.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h12">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h13">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h14">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h15">⍈</p></a>

        <p class="h17">上記の内容で更新してよろしいですか？</p>

        <c:if test="${errMsg != null}">
		  <p class="h19">${errMsg}</p>
		</c:if>

        <form action="UserUpdateServlet" method="post">
            <p class="h2">ログイン ID</p>
            <p class="h7">${user.loginId}</p>
            <input type="hidden" name="loginId" value="${user.loginId}">

            <p class="h3">ニックネーム</p>
            <input type="text" name="name" class="h8">

            <p class="h4">パスワード</p>
            <input type="password" name="password1" class="h9">

            <p class="h5">パスワード（確認）</p>
            <input type="password" name="password2" class="h10">

            <p class="h6">生年月日</p>
            <input type="date" name="birthdate" class="h11">

            <input type="submit" value="実行" class="h16">
        </form>
	</body>
</html>
