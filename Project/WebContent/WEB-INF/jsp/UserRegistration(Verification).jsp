<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー登録（確認）</title>
	<link rel="stylesheet" href="css/URV.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet"><p class="h12">«</p></a>

        <p class="h16">上記の内容で登録してよろしいですか？</p>

        <a href="UserRegistrationVerificationServlet?loginId=${userbeans.loginId}&name=${userbeans.name}&password=${userbeans.password}&birthDate=${userbeans.birthDate}">
        <input type="button" value="実行" class="h15"></a>

            <p class="h2">ログイン ID</p>
            <p class="h7">${userbeans.loginId}</p>

            <p class="h3">ニックネーム</p>
            <p class="h8">${userbeans.name}</p>

            <p class="h4">パスワード</p>
            <p class="h9">${userbeans.password}</p>

            <p class="h5">パスワード（確認）</p>
            <p class="h10">${userbeans.password}</p>

            <p class="h6">生年月日</p>
            <p class="h11">${userbeans.birthDate}</p>

	</body>
</html>