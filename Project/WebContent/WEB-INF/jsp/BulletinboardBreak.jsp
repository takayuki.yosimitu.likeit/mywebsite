<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <!-- jQueryの読込 -->
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- RateItの読込 ... （1） -->
<link href="js/RateIt/rateit.css" rel="stylesheet" type="text/css" />
<script src="js/RateIt/jquery.rateit.min.js"></script>

<script>
    $(function() {
        // RateItの設定 ...（2）
        $("#rateit1").rateit();
    });
</script>
	<title>掲示板（趣味）</title>
	<link rel="stylesheet" href="css/BBB.css">

</head>

	<body bgcolor=#E6FFE9>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>

        <div class="line1"></div>
        <p class="h2">${contents.contentsName}</p>

        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h9">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h10">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h11">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h12">⍈</p></a>

       <div class="box5">
	   <!-- tweetの表示欄 -->
        <c:forEach var="tweet" items="${TweetInfoList}">
        <div class="box3">
        <p class="h5">${tweet.name}</p>
        <p class="h7">${tweet.tweet}</p>
        <p class="h6">${tweet.formatDate}</p>
        <!-- 言い値の数 -->
		<div class="balloon"><span class="number">${tweet.goodId}</span></div>
        <!-- いいねマーク -->
		<div class="like"><a type="botton" name="good" href="GoodServlet?loginId=${userInfo.loginId}&contentsId=${contents.contentsId}&BtweetId=${tweet.btweetId}" class="btn">👍</a></div>
		</div><br>
        </c:forEach>
        </div>

        <c:if test="${errMsg != null}" >
        <a class=h8>${errMsg}</a>
　　　　　</c:if>

        <form action="BulletinBordBreak" method="post">
            <input type="hidden" name="name" value="${userInfo.name}">
	        <input type="hidden" name="loginId" value="${userInfo.loginId}">
            <input type="hidden" name="contentsId" value="${contents.contentsId}">

            <!-- 投稿欄 -->
            <textarea name="tweet" cols="15" rows="5" class="box"></textarea>

            <input type="submit" value="投稿" class="submit">
        </form>
	</body>

</html>
