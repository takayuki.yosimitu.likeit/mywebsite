package beans;

import java.io.Serializable;

public class GoodBeans implements Serializable{
	private String GoodId;
	private String UserId;
	private String LtweetId;
	private String CtweetId;
	private String BtweetId;

	public GoodBeans(String GoodId, String loginId, String ltweetId, String ctweetId, String btweetId) {
		this.GoodId = GoodId;
		this.UserId = loginId;
		this.LtweetId = ltweetId;
		this.CtweetId = ctweetId;
		this.BtweetId = btweetId;

	}

	public String getGoodId() {
		return GoodId;
	}

	public void setGoodId(String goodId) {
		GoodId = goodId;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getLtweetId() {
		return LtweetId;
	}

	public void setLtweetId(String ltweetId) {
		LtweetId = ltweetId;
	}

	public String getCtweetId() {
		return CtweetId;
	}

	public void setCtweetId(String ctweetId) {
		CtweetId = ctweetId;
	}

	public String getBtweetId() {
		return BtweetId;
	}

	public void setBtweetId(String btweetId) {
		BtweetId = btweetId;
	}
}
