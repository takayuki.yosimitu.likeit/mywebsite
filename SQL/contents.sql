# ************************************************************
# Sequel Pro SQL dump
# バージョン 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# ホスト: 127.0.0.1 (MySQL 5.7.31)
# データベース: LIKEIT
# 作成時刻: 2020-10-23 06:01:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# テーブルのダンプ contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contents`;

CREATE TABLE `contents` (
  `contents_id` int(11) NOT NULL,
  `contents_name` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`),
  UNIQUE KEY `contents_id` (`contents_id`),
  UNIQUE KEY `contents_name` (`contents_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;

INSERT INTO `contents` (`contents_id`, `contents_name`, `unit_id`)
VALUES
	(101,'Javaを始めよう',1),
	(102,'プログラミングの基本構',1),
	(103,'変数・式と演算子',1),
	(104,'条件分岐',1),
	(105,'繰り返し',1),
	(106,'配列',1),
	(107,'メソッド',1),
	(108,'複数のクラスを用いた開発',1),
	(109,'オブジェクト指向',1),
	(110,'インスタンスとクラス',1),
	(111,'さまざまなクラス機構',1),
	(112,'カプセル化',1),
	(113,'継承',1),
	(114,'高度な継承',1),
	(115,'多様性',1),
	(116,'標準クラス',1),
	(117,'例外',1),
	(118,'コレクション',1),
	(201,'DBを始めよう',2),
	(202,'SQL基礎',2),
	(203,'正規化',2),
	(204,'SQL応用',2),
	(205,'SQL総合問題',2),
	(301,'Webページの仕組み',3),
	(302,'サーブレットとJSPの基礎',3),
	(303,'Webアプリケーションの開発（基礎）',3),
	(304,'DBとの連携',3),
	(305,'モックの作り方',3),
	(306,'Web総合課題',3),
	(401,'バグ改修',4),
	(402,'追加実装',4),
	(501,'機能一覧考案・作成',5),
	(502,'モック作成',5),
	(503,'DB設計',5),
	(504,'Web実装',5),
	(505,'自己学習',5);

/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
