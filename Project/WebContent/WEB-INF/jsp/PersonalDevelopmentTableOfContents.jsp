<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>目次（個人開発）</title>
	<link rel="stylesheet" href="css/PDTOC.css">
</head>

	<body>
    <div class="line"></div>
        <p class="h1">LIKEIT</p>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h9">«</p></a>
        <a href="UserInformationServlet?loginId=${userInfo.loginId}"><p class="h10">☺</p></a>
        <a href="LoginServlet?loginId=${userInfo.loginId}"><p class="h11">✎</p></a>
        <a href="LogoutServlet?userInfo=${userInfo}"><p class="h12">⍈</p></a>

        <div class="line1"></div>
        <p class="h3">個人開発</p>


        <a href="BulletinBoardServlet?contentsId=501"><p class="h4">1.機能一覧考案・作成</p></a>
        <a href="BulletinBoardServlet?contentsId=502"><p class="h5">2.モック作成</p></a>
        <a href="BulletinBoardServlet?contentsId=503"><p class="h6">3.DB設計</p></a>
        <a href="BulletinBoardServlet?contentsId=504"><p class="h7">4.Web実装</p></a>
        <a href="BulletinBoardServlet?contentsId=505"><p class="h8">5.自己学習</p></a>

	</body>
</html>