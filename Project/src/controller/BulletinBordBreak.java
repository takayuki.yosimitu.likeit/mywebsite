package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BreakTweetBeans;
import beans.ContentsBeans;
import dao.BreakBulletinBoardDao;
import dao.TableOfContentsDao;

/**
 * Servlet implementation class BulletinBordBreak
 */
@WebServlet("/BulletinBordBreak")
public class BulletinBordBreak extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BulletinBordBreak() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの取得
				String name = request.getParameter("name");
				String tweet = request.getParameter("tweet");
				String loginId = request.getParameter("loginId");
				String contentsId = request.getParameter("contentsId");

				TableOfContentsDao TOCdao = new TableOfContentsDao();

				List<ContentsBeans> ContentsBeans = null;
				try {
					ContentsBeans = TOCdao.findByAll(contentsId);
				} catch (NoSuchAlgorithmException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			    for(ContentsBeans contents : ContentsBeans) {
			    	request.setAttribute("contents", contents);


				if(name == "" && tweet == "" && loginId == "" && contentsId == "") {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "入力されていません。");
					request.setAttribute("contents", contents);
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BulletinboardBreak.jsp");
					dispatcher.forward(request, response);
					return;
				}
			    }
			    BreakBulletinBoardDao BBBdao = new BreakBulletinBoardDao();
				try {
					BBBdao.InsertAll(name, tweet, loginId, contentsId);
				} catch (NoSuchAlgorithmException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				List<BreakTweetBeans> TweetInfoList = BBBdao.findAll(contentsId);
				request.setAttribute("TweetInfoList", TweetInfoList);
				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BulletinboardBreak.jsp");
				dispatcher.forward(request, response);
	            return;
	}
}