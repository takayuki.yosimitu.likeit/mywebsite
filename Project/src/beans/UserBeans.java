package beans;

import java.io.Serializable;

public class UserBeans implements Serializable{
		private int Id;
		private String LoginId;
		private String Name;
		private String BirthDate;
		private String Password;
		private String CreateDate;
		private String UpdateDate;

		// ログインIDを保存するためのコンストラクタ
			public UserBeans(String LoginId) {
				this.LoginId = LoginId;
			}

		// ログインセッションを保存するためのコンストラクタ
		public UserBeans(String LoginId, String Name) {
			this.LoginId = LoginId;
			this.Name = Name;
		}

		// 登録情報を保存ためのコンストラクタ
		public UserBeans(String LoginId, String Name, String birthdate, String Password) {
			this.LoginId = LoginId;
			this.Name = Name;
			this.BirthDate = birthdate;
			this.Password = Password;
		}

		// 全てのデータをセットするコンストラクタ
		public UserBeans(int Id, String LoginId, String Name, String birthDate, String Password, String CreateDate,
				String UpdateDate) {
			this.Id = Id;
			this.LoginId = LoginId;
			this.Name = Name;
			this.BirthDate = birthDate;
			this.Password = Password;
			this.CreateDate = CreateDate;
			this.UpdateDate = UpdateDate;
		}

		public int getId() {
			return Id;
		}

		public void setId(int id) {
			Id = id;
		}

		public String getLoginId() {
			return LoginId;
		}

		public void setLoginId(String loginId) {
			LoginId = loginId;
		}

		public String getName() {
			return Name;
		}

		public void setName(String name) {
			Name = name;
		}

		public String getBirthDate() {
			return BirthDate;
		}

		public void setBirthDate(String birthDate) {
			BirthDate = birthDate;
		}

		public String getPassword() {
			return Password;
		}

		public void setPassword(String password) {
			Password = password;
		}

		public String getCreateDate() {
			return CreateDate;
		}

		public void setCreateDate(String createDate) {
			CreateDate = createDate;
		}

		public String getUpdateDate() {
			return UpdateDate;
		}

		public void setUpdateDate(String updateDate) {
			UpdateDate = updateDate;
		}

}
