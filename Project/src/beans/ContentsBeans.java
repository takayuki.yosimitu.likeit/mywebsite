package beans;

import java.io.Serializable;

public class ContentsBeans implements Serializable{
	private int ContentsId;
	private String ContentsName;
	private int UnitId;

	public ContentsBeans(int ContentsId, String ContentsName, int UnitId) {
		this.ContentsId = ContentsId;
		this.ContentsName = ContentsName;
		this.UnitId = UnitId;
	}

	public ContentsBeans(int ContentsId, String ContentsName) {
		this.ContentsId = ContentsId;
		this.ContentsName = ContentsName;
	}

	public int getContentsId() {
		return ContentsId;
	}

	public void setContentsId(int contentsId) {
		ContentsId = contentsId;
	}

	public String getContentsName() {
		return ContentsName;
	}

	public void setContentsName(String contentsName) {
		ContentsName = contentsName;
	}

	public int getUnitId() {
		return UnitId;
	}

	public void setUnitId(int unitId) {
		UnitId = unitId;
	}


}
